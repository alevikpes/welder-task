import datetime
from unittest import mock

import factory
import pytest
from pytest_factoryboy import register

from bonus import Bonus
from salary import Salary


class BonusFactory(factory.Factory):

    class Meta:
        model = Bonus

    user_id = 1
    start_date = datetime.date(2021, 1, 1)
    end_date = datetime.date(2022, 1, 1)
    value = 100


class SalaryFactory(factory.Factory):

    class Meta:
        model = Salary

    user_id = 1
    start_date = datetime.date(2021, 1, 1)
    end_date = datetime.date(2022, 1, 1)
    _value = 1000
    salary = 1000
