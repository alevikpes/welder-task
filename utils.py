import datetime


def str_to_date(date_string: str) -> datetime.date:
    """Assuming the dates are strings."""
    return datetime.datetime.strptime(date_string, '%d-%m-%Y').date()
