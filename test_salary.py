import pytest


class TestSalary:

    def test_salary(self, salary_factory):
        # test getter
        assert salary_factory.salary == 1000
        # test setter
        salary_factory.salary = 2000
        assert salary_factory.salary == 2000
