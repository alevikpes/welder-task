import datetime

import pytest

from bonus_tracker import BonusTracker


class TestBonusTracker:

    @pytest.mark.parametrize(
        'i_start, i_end, bonuses, i_bonus',
        (
            ('01-01-2021', '01-01-2022', [100, 120], 100),
            ('01-07-2021', '01-01-2022', [100, 120], 50),
            ('01-07-2021', '01-05-2022', [100, 120], 90),
            ('01-11-2019', '01-05-2022', [120, 100, 100, 120], 260),
            # welder test
            ('01-07-2019', '01-01-2021', [100, 200, 100], 250),
        ),
    )
    def test_interval_bonus(self, i_start, i_end, bonuses, i_bonus):
        bt = BonusTracker(i_start, i_end)
        assert bt.get_interval_bonus(bonuses) == i_bonus

    @pytest.mark.parametrize(
        'i_start, i_end, y_bonus, i_bonus',
        (
            ('01-01-2021', '01-01-2022', 100, 100),
            ('01-07-2021', '01-01-2022', 100, 50),
            ('01-07-2021', '01-05-2022', 100, 50),
            ('01-11-2019', '01-05-2022', 120, 20),
        ),
    )
    def test_first_year_bonus(self, i_start, i_end, y_bonus, i_bonus):
        bt = BonusTracker(i_start, i_end)
        assert bt._first_year_bonus(y_bonus, bt.i_start.month) == i_bonus


    @pytest.mark.parametrize(
        'i_start, i_end, y_bonus, i_bonus',
        (
            ('01-01-2021', '01-01-2022', 200, 0),
            ('01-07-2021', '01-01-2022', 200, 0),
            ('01-07-2021', '01-05-2022', 120, 40),
            ('01-11-2019', '01-05-2022', 120, 40),
        ),
    )
    def test_last_year_bonus(self, i_start, i_end, y_bonus, i_bonus):
        bt = BonusTracker(i_start, i_end)
        assert bt._last_year_bonus(y_bonus, bt.i_end.month) == i_bonus
