
### Setup

Save the current code in a separate directory.
Depending on your local system setup, you may need to add the directory of
this project to the `PYTHONPATH` either in one of your `bashrc` files or in
your current shell session:
```bash
export PYTHONPATH=${PYTHONPATH}:<path/to/your/dir/>
```

`cd` in that directory and activate virtual environment. Install `pip-tools`
in this venv:
```bash
pip install pip-tools
```

Run:
```bash
pip-sync
```
to install the dependencies.

You are ready to work with the project.

Run:
```bash
pytest
```
for the tests.


### A: code

##### Step 1 and Step 2
For this part code is in `bonus.py`, `salary.py` and `bonus_tracker.py`.

##### Step 3
**NOT IMPLEMENTED**

This should be pretty easy to filter with an ORM, which is normally used
in this case for the communication with a db.


### B: database

##### Scan
```python
id: int | uuid
comment: text | string
```

##### Dimension
```python
id: int | uuid
weight: float
comment: text | string
```

##### Aspect
```python
id: int | uuid
weight: float
comment: text | string
```

##### Question
```python
id: int | uuid
q_text: text | string
answer_id: Choices(Answer.key)
comment: text | string
```

##### Answer
Answer can be an Enum object with lots of fields.
```python
class Answers(Enum):
    <id> = <a_text>
```

##### UserAnswer
```python
id: int | uuid
scan_id: ForegnKey(Scan) | ManyToMany(Scan)
dimension_id: ForegnKey(Dimension) | ManyToMany(Dimension)
aspect_id: ForegnKey(Aspect) | ManyToMany(Aspect)
question_id: ForegnKey(Question) | ManyToMany(Question)
answer_id: Answer.key
comment: text | string
```
