import datetime

import utils


class BonusTracker:

    def __init__(self, interval_start_date, interval_end_date):
        self.i_start = utils.str_to_date(interval_start_date)
        self.i_end = utils.str_to_date(interval_end_date)

    def get_interval_bonus(self, yearly_bonuses: list):
        """Calculate bonus for a user in the specified interval.

        :param yearly_bonuses: - A list of bonuses as Bonus objects.
        :return float:
        """
        # months in the first interval year
        bonus_y_first = self._first_year_bonus(yearly_bonuses[0],
                                               self.i_start.month)
        # months in the last interval year
        bonus_y_last = self._last_year_bonus(yearly_bonuses[-1],
                                             self.i_end.month)
        # There should be exactly multiple of 12 months left after
        # removing the first and the last years.
        i_bonus = bonus_y_first + bonus_y_last
        for i in range(len(yearly_bonuses)):
            if i > 0 and i < (len(yearly_bonuses) - 1):

                # NOTE for step 2:
                # If the bonus is a salary rate, then we need to get the user's
                # salary and find the bonus in the specified period, which then
                # should be applied further in the algorithm.

                i_bonus += yearly_bonuses[i]

        return i_bonus

    def _first_year_bonus(self, y_bonus, i_months):
        months = 12 - i_months + 1

        # NOTE for step 2:
        # If the bonus is a salary rate, then we need to get the user's
        # salary and find the bonus in the specified period, which then
        # should be applied further in the algorithm.

        return months * y_bonus / 12

    def _last_year_bonus(self, y_bonus, i_months):
        months = i_months - 1  # last month not included

        # NOTE for step 2:
        # If the bonus is a salary rate, then we need to get the user's
        # salary and find the bonus in the specified period, which then
        # should be applied further in the algorithm.

        return months * y_bonus / 12
