import datetime

import pytest
from pytest_factoryboy import register

import factories


register(factories.BonusFactory)
register(factories.SalaryFactory)


@pytest.fixture
def str_to_date_fixture():
    """Convert string date in format `31-12-2022` to datetime.Date object."""

    def _str_to_date(date_str: str) -> datetime.date:
        return datetime.datetime.strptime(date_str, '%d-%m-%Y').date()

    return _str_to_date
