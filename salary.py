import datetime


# Table: salary
# user_id: int | uuid
# start_date: date
# end_date: date
# value: float


class Salary:

    _value = None

    def __init__(
        self,
        start_date: datetime.date,
        end_date: datetime.date,
        value: float,
        user_id: int,
    ):
        self.start_date = start_date
        self.end_date = end_date
        self._value = value
        self.user_id = user_id

    @property
    def salary(self):
        return self._value

    @salary.setter
    def salary(self, val):
        self._value = val
