import datetime


# Table: bonus
# start_date: date
# end_date: date
# value: float
# bonus_rate: bool
# user_id: int | uuid


class Bonus:

    def __init__(
        self,
        start_date: datetime.date,
        end_date: datetime.date,
        value: float,
        user_id: int,
        is_salary_rate: bool=False,
    ):
        self.start_date = start_date
        self.end_date = end_date
        self.value = value
        self.user_id = user_id
        self.is_salary_rate = is_salary_rate
