import pytest

import utils


def test_str_to_date(str_to_date_fixture):
    date_str = '01-01-2022'
    assert utils.str_to_date(date_str) == str_to_date_fixture(date_str)
